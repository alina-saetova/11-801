public interface UsersService {
    void signIn(String login, String password);
    void signUp(String firstName, String lastName, String login, String password);
}
